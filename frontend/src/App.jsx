import { Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import { Box } from "@mui/material";
import RegisterPage from "./pages/RegisterPage";
import LoginPage from "./pages/LoginPage";
import HardPage from "./pages/HardPage";
import HardPageDetail from "./pages/HardPageDetail";
import SoftPage from "./pages/SoftPage";
import SoftPageDetail from "./pages/SoftPageDetail";
import ProfilePage from "./pages/ProfilePage";

function App() {
    return (
        <Box
            sx={{
                display: "flex",
                flexDirection: "column",
                minHeight: "100vh",
            }}
        >
            <Navbar />
            <Routes>
                <Route path='/hard' element={<HardPage />}/>
                <Route path='/hard/:id' element={<HardPageDetail />}/>
                <Route path='/soft' element={<SoftPage />}/>
                <Route path='/soft/:id' element={<SoftPageDetail />}/>
                <Route path='/login' element={<LoginPage />}/>
                <Route path='/profile' element={<ProfilePage />}/>
            </Routes>
            <Footer />
        </Box>
    );
}

export default App;
