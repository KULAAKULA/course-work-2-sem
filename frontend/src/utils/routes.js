export const ROUTES = [
    {name: 'Hard skills', link: '/hard'},
    {name: 'Soft skills', link: '/soft'},
]