import React, { useState } from "react";
import { Box, Typography, Rating } from "@mui/material";

const SoftStars = (props) => {
    const [value, setValue] = useState(3);
    return (
        <Box style={{ margin: "30px 0" }}>
            <Typography variant="h5">{props.crit.title}</Typography>
            <Typography variant="subtitle1" color="gray">{props.crit.description}</Typography>
            <Rating
                key={parseInt(props.idx)}
                value={value}
                onChange={(event, newValue) => {
                    let ans = props.answers.slice()
                    ans[props.idx].mark = newValue
                    props.setAnswers(ans)
                    setValue(newValue);
                }}
            />
        </Box>
    );
};

export default SoftStars;
