import { Container, CircularProgress } from "@mui/material";
import React from "react";

const Loader = () => {
    return (
        <Container align="center">
            <CircularProgress />
        </Container>
    );
};

export default Loader;
