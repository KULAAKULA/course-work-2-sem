import {
    CardContent,
    Typography,
    CardActions,
    Button,
    Box,
    Card,
    CardMedia
} from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";

const SoftSkillCard = ({id, first_name, middle_name, last_name, photo }) => {
    let navigate = useNavigate()
    return (
        <Box sx={{ maxWidth: 250 }}>
            <Card variant="outlined">
                <CardMedia
                    component="img"
                    height="140"
                    image={import.meta.env.VITE_API_URL+photo}
                    alt={`${last_name} ${first_name} ${middle_name}`}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                    {last_name} {first_name} {middle_name}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" onClick={() => navigate(`/soft/${id}`)}>Пройти тестирование</Button>
                </CardActions>
            </Card>
        </Box>
    );
};

export default SoftSkillCard;
