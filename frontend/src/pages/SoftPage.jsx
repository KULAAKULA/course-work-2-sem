import React, { useState, useEffect } from "react";
import { Container, Grid, Typography, Link } from "@mui/material";
import { Link as RouterLink } from "react-router-dom";
import instance from "../utils/axios";
import Loader from "../components/Loader";
import SoftSkillCard from "../components/SoftSkillCard";

const SoftPage = () => {
    const [skills, setSkills] = useState([]);
    const [loading, setLoading] = useState(false);
    const [status401, setStatus401] = useState(false);
    const [error, setError] = useState(false);

    useEffect(() => {
        setLoading(true);
        instance
            .get("/api/soft-skills/surveys/", {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("access")}`,
                },
            })
            .then((res) => res.data)
            .then((data) => setSkills(data))
            .catch((err) => {
                if (err.response.status === 401) {
                    setStatus401(true);
                } else {
                    setError(true);
                }
            });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    if (loading) return <Loader />;

    return (
        <Container>
            <Typography variant="h2" gutterBottom textAlign="center">
                Soft skills
            </Typography>
            {status401 && (
                <Typography variant="h3" textAlign="center">
                    Вам необоходимо{" "}
                    <Link component={RouterLink} to="/login">
                        авторизоваться
                    </Link>
                </Typography>
            )}
            {error && (
                <Typography variant="h3" textAlign="center">
                    Непредвиденная ошибка
                </Typography>
            )}
            {!status401 && !error && skills.length === 0 && <Typography variant="h4" textAlign="center">Некого оценивать</Typography>}
            <Grid container spacing={4}>
                {skills?.map(({ id, cow_data }) => (
                    <Grid item key={id} xs={12} sm={6} md={4}>
                        <SoftSkillCard
                            id={id}
                            first_name={cow_data.first_name}
                            last_name={cow_data.last_name}
                            middle_name={cow_data.middle_name}
                            photo={cow_data.photo}
                        />
                    </Grid>
                ))}
            </Grid>
        </Container>
    );
};

export default SoftPage;
