import { Container, Typography, Rating, Box, Button } from "@mui/material";
import React, { useState, useEffect } from "react";
import { useParams, Navigate } from "react-router-dom";
import instance from "../utils/axios";
import Loader from "../components/Loader";
import HardStars from "../components/HardStars";

const HardPageDetail = () => {
    let { id } = useParams();
    const [skill, setSkill] = useState([]);
    const [loading, setLoading] = useState(false);
    const [status401, setStatus401] = useState(false);
    const [error, setError] = useState(false);
    const [answers, setAnswers] = useState([]);
    const [redirect, setRedirect] = useState(false)

    useEffect(() => {
        setLoading(true);
        instance
            .get(`/api/hard-skills/surveys/${id}`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("access")}`,
                },
            })
            .then((res) => res.data)
            .then((data) => {
                let ans = [];
                setSkill(data);
                data?.crit_data?.map((crit) => {
                    ans.push({
                        skill: parseInt(id),
                        mark: 3,
                        criteria: crit.id,
                    });
                });
                setAnswers(ans);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    setStatus401(true);
                } else {
                    setError(true);
                }
            });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    const submit = (e) => {
        e.preventDefault();
        answers.map((answer) => {
            instance
                .post(
                    `/api/hard-skills/answers/`,
                    {
                        criteria: answer.criteria,
                        mark: answer.mark,
                        skill: answer.skill,
                    },
                    {
                        headers: {
                            Authorization: `Bearer ${localStorage.getItem(
                                "access",
                            )}`,
                        },
                    },
                )
                .then((res) => res.data)
                .then((data) => console.log(data));
        });
        setRedirect(true)

    };

    if (loading) return <Loader />;

    if (redirect) return <Navigate to='/' replace={true} />

    return (
        <Container>
            {status401 && (
                <Typography variant="h3" textAlign="center">
                    Вам необоходимо{" "}
                    <Link component={RouterLink} to="/login">
                        авторизоваться
                    </Link>
                </Typography>
            )}
            {error && (
                <Typography variant="h3" textAlign="center">
                    Непредвиденная ошибка
                </Typography>
            )}
            {!status401 && !error && (
                <>
                    <Typography variant="h3" textAlign="center">
                        {skill.cow_data?.last_name} {skill.cow_data?.first_name}{" "}
                        {skill.cow_data?.middle_name}
                    </Typography>
                    <Container maxWidth="sm">
                        {skill?.crit_data?.map((crit, idx) => {
                            return (
                                <HardStars
                                    crit={crit}
                                    key={idx}
                                    idx={idx}
                                    answers={answers}
                                    setAnswers={setAnswers}
                                />
                            );
                        })}
                        <Button variant="outlined" onClick={submit}>
                            Отправить
                        </Button>
                    </Container>
                </>
            )}
        </Container>
    );
};

export default HardPageDetail;
