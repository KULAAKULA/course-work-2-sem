import React from 'react';
import {
    Box,
    Typography,
    Avatar,
    TextField,
    Button,
    Grid,
    Link,
    Container
} from "@mui/material";
import {Link as RouterLink, Navigate} from 'react-router-dom'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import axios from "axios";
import user from "../store/user";

const LoginPage = () => {
    const [login, setLogin] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [loginError, setLoginError] = React.useState(false)
    const [passwordError, setPasswordError] = React.useState(false)
    const [redirect,setRedirect] = React.useState(false)

    const handleSubmit = (e) => {
        e.preventDefault()
        if (login.length < 1) {
            setLoginError(true)
        } else {
            setLoginError(false)
        }
        if (password.length < 1) {
            setPasswordError(true)
        } else {
            setPasswordError(false)
        }

        if (!loginError && !passwordError) {
            user.getUser() ? setRedirect(true) :
            axios.post(`${import.meta.env.VITE_API_URL}/api/user/login/`, {
                login, password
            }, {
                headers: {
                    "Content-Type": "application/json"
                },
                withCredentials: true
            })
                .then(res => res.data)
                .then(({access}) => {
                    localStorage.setItem('access', access)
                    axios.get(`${import.meta.env.VITE_API_URL}/api/user/user/`, {
                        headers: {
                            "Content-Type": "application/json",
                            "Authorization": `Bearer ${access}`,
                        },
                        withCredentials: true
                    })
                        .then(res => res.data)
                        .then(data => user.setUser(data))
                        .then(() => setRedirect(true))
                })
        }
    }

    if (redirect) return <Navigate to='/' replace={true} />

    return (
        <Container>
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <Box component="form" noValidate onSubmit={handleSubmit} sx={{mt: 3}}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                value={login}
                                onChange={(e) => setLogin(e.target.value)}
                                required
                                fullWidth
                                id="login"
                                label="Login"
                                name="login"
                                error={loginError}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="new-password"
                                error={passwordError}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Sign Up
                    </Button>
                    <Grid container justifyContent="flex-end">
                        <Grid item>
                            <Link component={RouterLink} to="/register" variant="body2">
                                Don't have an account? Create it
                            </Link>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
        </Container>
    );
};

export default LoginPage;