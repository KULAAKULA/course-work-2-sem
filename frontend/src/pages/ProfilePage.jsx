import React, { useState, useEffect } from "react";
import {
    Container,
    Link,
    Typography,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Paper,
} from "@mui/material";
import Loader from "../components/Loader";
import instance from "../utils/axios";

const ProfilePage = () => {
    const [hardSkills, setHardSkills] = useState([]);
    const [softSkills, setSoftSkills] = useState([]);
    const [loading, setLoading] = useState(false);
    const [status401, setStatus401] = useState(false);
    const [error, setError] = useState(false);

    useEffect(() => {
        setLoading(true);
        instance
            .get("/api/hard-skills/answers/by_user/", {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("access")}`,
                },
            })
            .then((res) => res.data)
            .then((data) => {
                let res = [];
                for (let key in data) {
                    res.push({ key: key, value: data[key] });
                }
                setHardSkills(res);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    setStatus401(true);
                } else {
                    setError(true);
                }
            });
        instance
            .get("/api/soft-skills/answers/by_user/", {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("access")}`,
                },
            })
            .then((res) => res.data)
            .then((data) => {
                let res = [];
                for (let key in data) {
                    res.push({ key: key, value: data[key] });
                }
                setSoftSkills(res);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    setStatus401(true);
                } else {
                    setError(true);
                }
            });
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, []);

    if (loading) return <Loader />;

    return (
        <Container>
            <Typography variant="h2" gutterBottom textAlign="center">
                Как вас оценили
            </Typography>
            {status401 && (
                <Typography variant="h3" textAlign="center">
                    Вам необоходимо{" "}
                    <Link component={RouterLink} to="/login">
                        авторизоваться
                    </Link>
                </Typography>
            )}
            {error && (
                <Typography variant="h3" textAlign="center">
                    Непредвиденная ошибка
                </Typography>
            )}
            <Typography variant="h4" gutterBottom textAlign="center">
                Hard skills
            </Typography>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Критерий</TableCell>
                            <TableCell align="right">Средняя оценка</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {hardSkills.map((skill) => {
                            let backgroundColor = "";
                            if (parseFloat(skill.value) > 3.5) {
                                backgroundColor = "#00b30095";
                            } else if (
                                parseFloat(skill.value) > 1.5 &&
                                parseFloat(skill.value) <= 3.5
                            ) {
                                backgroundColor = "#ffff0095";
                            } else {
                                backgroundColor = "#ff000095";
                            }
                            return (
                                <TableRow
                                    key={skill.key}
                                    sx={{
                                        "&:last-child td, &:last-child th": {
                                            border: 0,
                                        },
                                        backgroundColor,
                                    }}
                                >
                                    <TableCell component="th" scope="row">
                                        {skill.key}
                                    </TableCell>
                                    <TableCell align="right">
                                        {skill.value}
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <Typography variant="h4" gutterBottom textAlign="center" style={{marginTop: '20px'}}>
                Soft skills
            </Typography>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Критерий</TableCell>
                            <TableCell align="right">Средняя оценка</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {softSkills.map((skill) => {
                            let backgroundColor = "";
                            if (parseFloat(skill.value) > 3.5) {
                                backgroundColor = "#00b30095";
                            } else if (
                                parseFloat(skill.value) > 1.5 &&
                                parseFloat(skill.value) <= 3.5
                            ) {
                                backgroundColor = "#ffff0095";
                            } else {
                                backgroundColor = "#ff000095";
                            }
                            return (
                                <TableRow
                                    key={skill.key}
                                    sx={{
                                        "&:last-child td, &:last-child th": {
                                            border: 0,
                                        },
                                        backgroundColor,
                                    }}
                                >
                                    <TableCell component="th" scope="row">
                                        {skill.key}
                                    </TableCell>
                                    <TableCell align="right">
                                        {skill.value}
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    );
};

export default ProfilePage;
