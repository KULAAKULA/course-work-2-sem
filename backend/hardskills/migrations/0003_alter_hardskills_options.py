# Generated by Django 4.0.5 on 2022-07-10 10:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardskills', '0002_alter_hardskills_options_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hardskills',
            options={'verbose_name': 'Опрос', 'verbose_name_plural': 'Опросы'},
        ),
    ]
