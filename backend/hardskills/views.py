from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from hardskills.models import HardSkills, HardSkillsAnswer, HardSkillsCriteria
from hardskills.serializers import HardSkillsSerializer, HardSkillsAnswerSerializer, HardSkillsCriteriaSerializer
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied, NotFound
from rest_framework.status import HTTP_201_CREATED
import django_filters.rest_framework


class HardSkillsViewSet(ModelViewSet):
    queryset = HardSkills.objects.all()
    serializer_class = HardSkillsSerializer
    permission_classes = [IsAuthenticated]


    def list(self, request):
        data = self.queryset.filter(respondent=request.user, completed=False)
        return Response(self.serializer_class(data, many=True).data)

    def retrieve(self, request, pk=None):
        try:
            data = self.queryset.get(id=pk)
        except HardSkills.DoesNotExist:
            raise NotFound(detail={'message': 'Survey was not found'})
        if data.respondent != request.user:
            raise PermissionDenied(detail={'message': 'You have no access to this test'})
        if data.completed:
            raise PermissionDenied(detail={'message': 'You have already completed this test'})
        return Response(self.serializer_class(data).data)


class HardSkillsAnswerViewSet(ModelViewSet):
    queryset = HardSkillsAnswer.objects.all()
    serializer_class = HardSkillsAnswerSerializer
    permission_classes_by_action = {'create': [IsAuthenticated]}
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['mark']

    @action(methods=['GET'], url_path='by_user', detail=False, permission_classes=[IsAuthenticated])
    def get(self, request):
        skills = HardSkills.objects.filter(coworker=request.user)
        data = {}
        for skill in skills:
            answers = self.queryset.filter(skill=skill)
            for answer in answers:
                if f'{answer.criteria.title}_mark' not in data.keys():
                    data[f'{answer.criteria.title}_mark'] = answer.mark
                    data[f'{answer.criteria.title}_count'] = 1
                else:
                    data[f'{answer.criteria.title}_mark'] += answer.mark
                    data[f'{answer.criteria.title}_count'] += 1
        resp = {}
        for key, value in data.items():
            dat = key.split('_')
            if dat[1] == 'mark':
                resp[f'{dat[0]}'] = value
            elif dat[1] == 'count':
                resp[f'{dat[0]}'] /= value
        return Response(resp)

    def create(self, request):
        crit = HardSkillsCriteria.objects.get(id=request.data['criteria'])
        skill = HardSkills.objects.get(id=request.data['skill'])
        mark = request.data['mark']
        try:
            self.queryset.get(criteria=crit, skill=skill)
            raise PermissionDenied(detail={'message': 'answer already given'})
        except HardSkillsAnswer.DoesNotExist:
            pass
        skill.completed = True
        skill.save()
        self.queryset.create(criteria=crit, skill=skill, mark=mark)
        return Response({'message': 'success'}, status=HTTP_201_CREATED)

    def destroy(self, request):
        id = HardSkillsAnswer.objects.get(id=request.data['id'])
        print(id)
        return Response({'message': 'success'})


    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

class HardSkillsCriteriaViewSet(ModelViewSet):
    queryset = HardSkillsCriteria.objects.all()
    serializer_class = HardSkillsCriteriaSerializer
