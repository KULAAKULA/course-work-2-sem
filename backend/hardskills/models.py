from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from simple_history.models import HistoricalRecords

from user.models import User


class HardSkills(models.Model):
    respondent = models.ForeignKey(to=User, verbose_name='Опрашиваемый', on_delete=models.CASCADE,
                                   related_name='skill_resp')
    coworker = models.ForeignKey(to=User, verbose_name='О ком опрос', on_delete=models.CASCADE,
                                 related_name='skill_cow')
    criteria = models.ManyToManyField(to='HardSkillsCriteria', verbose_name='Критерии', related_name='hard_skills')
    completed = models.BooleanField(verbose_name='Выполнено?', default=False)
    history = HistoricalRecords()

    def __str__(self):
        return f'{self.respondent.first_name} {self.respondent.last_name} - ' \
               f'{self.coworker.first_name} {self.coworker.last_name}'

    class Meta:
        verbose_name = 'Опрос'
        verbose_name_plural = 'Опросы'


class HardSkillsCriteria(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=255)
    description = models.TextField(verbose_name='Описание')
    history = HistoricalRecords()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Критерий'
        verbose_name_plural = 'Критерии'


class HardSkillsAnswer(models.Model):
    criteria = models.ForeignKey(to='HardSkillsCriteria', verbose_name='Критерии', on_delete=models.CASCADE)
    mark = models.IntegerField(verbose_name='Оценка', validators=(MinValueValidator(1), MaxValueValidator(5)),
                               default=3)
    skill = models.ForeignKey(to=HardSkills, verbose_name='Скилл', on_delete=models.CASCADE)
    history = HistoricalRecords()

    def __str__(self):
        return f'{self.id}'

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'
