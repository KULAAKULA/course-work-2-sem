from rest_framework.serializers import ModelSerializer
from hardskills.models import HardSkills, HardSkillsAnswer, HardSkillsCriteria
from user.serializers import UserSerializer


class HardSkillsAnswerSerializer(ModelSerializer):
    class Meta:
        model = HardSkillsAnswer
        fields = '__all__'


class HardSkillsCriteriaSerializer(ModelSerializer):
    class Meta:
        model = HardSkillsCriteria
        fields = '__all__'


class HardSkillsSerializer(ModelSerializer):
    resp_data = UserSerializer(source='respondent')
    cow_data = UserSerializer(source='coworker')
    crit_data = HardSkillsCriteriaSerializer(source='criteria', many=True)

    class Meta:
        model = HardSkills
        exclude = ['respondent', 'coworker', 'criteria']
