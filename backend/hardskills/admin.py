from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from simple_history.admin import SimpleHistoryAdmin

from .models import HardSkills, HardSkillsAnswer, HardSkillsCriteria


@admin.register(HardSkills)
class HardSkillsAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    pass


@admin.register(HardSkillsAnswer)
class HardSkillsAnswerAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    pass


@admin.register(HardSkillsCriteria)
class HardSkillsCriteriaAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    pass
