from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Q
from rest_framework.exceptions import AuthenticationFailed, ParseError, ValidationError, NotFound
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import update_last_login
from user.models import User
from user.serializers import UserSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework_simplejwt.exceptions import TokenError


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(methods=['POST'], detail=False)
    def login(self, request):
        if 'login' not in request.data:
            data = {'login': ['Login must be provided']}
            if 'password' not in request.data:
                data['password'] = ['Password must be provided']
            raise ValidationError(data)
        if 'password' not in request.data:
            raise ValidationError({'password': ['Password must be provided']})

        try:
            user = self.queryset.get(login=request.data.get('login'))
        except User.DoesNotExist:
            raise NotFound({'message': 'User with provided credentials does not exist'})

        if not user.check_password(request.data.get('password')):
            raise AuthenticationFailed({'message': 'Incorrect password'})

        refresh = RefreshToken.for_user(user)
        update_last_login(None, user)
        response = Response()
        response.set_cookie('refresh', str(refresh))
        response.data = {'access': str(refresh.access_token)}
        return response

    @action(methods=['POST'], detail=False,
            permission_classes=[IsAuthenticated])
    def logout(self, request):
        response = Response()
        if 'refresh' not in request.COOKIES or len(request.COOKIES['refresh']) < 1:
            raise AuthenticationFailed({'message': 'Unauthenticated'})

        response.delete_cookie('refresh')
        response.data = {'message': 'success'}
        return response

    @action(methods=['PATCH'], detail=False,
            permission_classes=[IsAuthenticated], url_path='update')
    def user_update(self, request):
        user = request.user
        data = request.data
        if 'login' in data and len(data.get('login')) > 0:
            try:
                self.queryset.get(
                    login=data['login'])
                raise ParseError({'message': 'Login already taken'})
            except User.DoesNotExist:
                pass
            user.login = data['login']
            user.save()

        return Response({'message': 'success'})

    @action(methods=['GET'], detail=False,
            permission_classes=[IsAuthenticated])
    def user(self, request):
        if 'refresh' not in request.COOKIES or len(request.COOKIES['refresh']) < 1:
            raise AuthenticationFailed({'message': 'Unauthenticated'})
        user = request.user
        data = UserSerializer(user).data
        return Response(data)

    def list(self, request):
        data = self.queryset.filter(Q(date_of_birth='2003-05-09') | Q(date_of_birth='2003-01-19'))
        return Response(self.serializer_class(data, many=True).data)
