from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from simple_history.admin import SimpleHistoryAdmin

from .models import SoftSkills, SoftSkillsAnswer, SoftSkillsCriteria


@admin.register(SoftSkills)
class SoftSkillsAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    pass


@admin.register(SoftSkillsAnswer)
class SoftSkillsAnswerAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    pass


@admin.register(SoftSkillsCriteria)
class SoftSkillsAnswerAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    pass
