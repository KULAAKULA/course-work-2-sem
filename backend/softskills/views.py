from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from softskills.models import SoftSkills, SoftSkillsAnswer, SoftSkillsCriteria
from softskills.serializers import SoftSkillsSerializer, SoftSkillsAnswerSerializer, SoftSkillsCriteriaSerializer
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied, NotFound
from rest_framework.status import HTTP_201_CREATED


class SoftSkillsViewSet(ModelViewSet):
    queryset = SoftSkills.objects.all()
    serializer_class = SoftSkillsSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request):
        data = self.queryset.filter(respondent=request.user, completed=False)
        return Response(self.serializer_class(data, many=True).data)

    def retrieve(self, request, pk=None):
        try:
            data = self.queryset.get(id=pk)
        except SoftSkills.DoesNotExist:
            raise NotFound(detail={'message': 'Survey was not found'})
        if data.respondent != request.user:
            raise PermissionDenied(detail={'message': 'You have no access to this test'})
        if data.completed:
            raise PermissionDenied(detail={'message': 'You have already completed this test'})
        return Response(self.serializer_class(data).data)


class SoftSkillsAnswerViewSet(ModelViewSet):
    queryset = SoftSkillsAnswer.objects.all()
    serializer_class = SoftSkillsAnswerSerializer
    permission_classes_by_action = {'create': [IsAuthenticated]}

    @action(methods=['GET'], url_path='by_user', detail=False, permission_classes=[IsAuthenticated])
    def get(self, request):
        skills = SoftSkills.objects.filter(coworker=request.user)
        data = {}
        for skill in skills:
            answers = self.queryset.filter(skill=skill)
            for answer in answers:
                if f'{answer.criteria.title}_mark' not in data.keys():
                    data[f'{answer.criteria.title}_mark'] = answer.mark
                    data[f'{answer.criteria.title}_count'] = 1
                else:
                    data[f'{answer.criteria.title}_mark'] += answer.mark
                    data[f'{answer.criteria.title}_count'] += 1
        resp = {}
        for key, value in data.items():
            dat = key.split('_')
            if dat[1] == 'mark':
                resp[f'{dat[0]}'] = value
            elif dat[1] == 'count':
                resp[f'{dat[0]}'] /= value
        return Response(resp)

    def create(self, request):
        crit = SoftSkillsCriteria.objects.get(id=request.data['criteria'])
        skill = SoftSkills.objects.get(id=request.data['skill'])
        mark = request.data['mark']
        try:
            self.queryset.get(criteria=crit, skill=skill)
            raise PermissionDenied(detail={'message': 'answer already given'})
        except SoftSkillsAnswer.DoesNotExist:
            pass
        skill.completed = True
        skill.save()
        self.queryset.create(criteria=crit, skill=skill, mark=mark)
        return Response({'message': 'success'}, status=HTTP_201_CREATED)

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

class SoftSkillsCriteriaViewSet(ModelViewSet):
    queryset = SoftSkillsCriteria.objects.all()
    serializer_class = SoftSkillsCriteriaSerializer
