from rest_framework.serializers import ModelSerializer
from softskills.models import SoftSkills, SoftSkillsAnswer, SoftSkillsCriteria
from user.serializers import UserSerializer


class SoftSkillsAnswerSerializer(ModelSerializer):
    class Meta:
        model = SoftSkillsAnswer
        fields = '__all__'


class SoftSkillsCriteriaSerializer(ModelSerializer):
    class Meta:
        model = SoftSkillsCriteria
        fields = '__all__'


class SoftSkillsSerializer(ModelSerializer):
    resp_data = UserSerializer(source='respondent')
    cow_data = UserSerializer(source='coworker')
    crit_data = SoftSkillsCriteriaSerializer(source='criteria', many=True)

    class Meta:
        model = SoftSkills
        exclude = ['respondent', 'coworker', 'criteria']
