from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from django.conf import settings
from django.conf.urls.static import static
from hardskills.views import HardSkillsViewSet, HardSkillsAnswerViewSet, HardSkillsCriteriaViewSet
from softskills.views import SoftSkillsViewSet, SoftSkillsAnswerViewSet, SoftSkillsCriteriaViewSet
from user.views import UserViewSet

router = DefaultRouter()
router.register('hard-skills/surveys', HardSkillsViewSet)
router.register('hard-skills/answers', HardSkillsAnswerViewSet)
router.register('hard-skills/criterias', HardSkillsCriteriaViewSet)
router.register('soft-skills/surveys', SoftSkillsViewSet)
router.register('soft-skills/answers', SoftSkillsAnswerViewSet)
router.register('soft-skills/criterias', SoftSkillsCriteriaViewSet)
router.register('user', UserViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls))
]

urlpatterns += static(settings.MEDIA_URL,
                        document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL,
                        document_root=settings.STATIC_ROOT)